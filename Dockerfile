# Ripped off from https://github.com/sameersbn/docker-squid

FROM ubuntu:18.10

ENV \
  DEBIAN_FRONTEND=noninteractive \
  SQUID_CACHE_DIR=/var/spool/squid \
  SQUID_LOG_DIR=/var/log/squid \
  SQUID_USER=proxy \
  SQUID_VERSION=4.6

RUN echo 'deb http://security.ubuntu.com/ubuntu cosmic-security main' \
  > /etc/apt/sources.list.d/security.list

RUN \
  echo -ne '' \
  && apt-get update \
  && apt-get install -y \
    build-essential \
    curl \
    libssl1.0-dev

ENV \
  SQUID_DOWNLOAD_FOLDER_NAME=squid-${SQUID_VERSION}

ENV \
  SQUID_DOWNLOAD_FILENAME=${SQUID_DOWNLOAD_FOLDER_NAME}.tar.gz

ENV \
  SQUID_DOWNLOAD_URL=http://www.squid-cache.org/Versions/v4/squid-4.6.tar.gz

RUN \
  echo -ne '' \
  && curl -o ${SQUID_DOWNLOAD_FILENAME} ${SQUID_DOWNLOAD_URL} \
  && rm -rf /var/lib/apt/lists/* \
  && tar -xf ${SQUID_DOWNLOAD_FILENAME} \
  && cd ${SQUID_DOWNLOAD_FOLDER_NAME} \
  && ./configure \
    --enable-http-violations \
    --enable-ssl-crtd \
    --prefix '' \
    --with-openssl \
  && make -j6 \
  && make install

RUN \
  mkdir -p /etc/squid

ENV CERTS_DIR=/var/certs

COPY entrypoint.sh /sbin/entrypoint.sh
COPY squid.conf /etc/squid/squid.conf
COPY regex.whitelist /etc/squid/regex.whitelist
COPY certs-build.sh /certs-build.sh
COPY certs.conf /certs.conf

EXPOSE 3128/tcp
ENTRYPOINT ["/sbin/entrypoint.sh"]
