#!/usr/bin/env bash

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

main() {
  until [ -f '/usr/local/share/ca-certificates/cert.crt' ]; do
    sleep 1
  done

  update-ca-certificates

  if [ $# -eq 0 ]; then
    /sbin/run-tests
  else
    exec "$@"
  fi
}

main "$@"
