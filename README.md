# Docker-squid proxy

## Objective

Provide a simple, minimalist container for regulating http and https requests by routing them through a proxy.

## Justification

Maintain a semblance of security for docker containers which may otherwise phone home.

## Credit

Major components were borrowed from https://github.com/sameersbn/docker-squid
