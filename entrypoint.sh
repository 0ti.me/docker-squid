#!/usr/bin/env bash

# Ripped off from https://github.com/sameersbn/docker-squid

set -e

SQUID="/sbin/squid"

create_log_dir() {
  mkdir -p ${SQUID_LOG_DIR}
  chmod -R 755 ${SQUID_LOG_DIR}
  chown -R ${SQUID_USER}:${SQUID_USER} ${SQUID_LOG_DIR}
}

create_cache_dir() {
  mkdir -p ${SQUID_CACHE_DIR}
  chown -R ${SQUID_USER}:${SQUID_USER} ${SQUID_CACHE_DIR}
}

create_log_dir
create_cache_dir

if [ ! -f '/var/certs/cert.pem' ]; then
  /certs-build.sh
fi

SSL_DB=/var/lib/ssl_db

if [ ! -e "${SSL_DB}" ]; then
  /libexec/security_file_certgen -c -s "${SSL_DB}" -M 64MB
fi

chown -R proxy "${SSL_DB}"

# allow arguments to be passed to squid
if [[ ${1:0:1} = '-' ]]; then
  EXTRA_ARGS="$@"
  set --
elif [[ ${1} == squid || ${1} == $(which squid) ]]; then
  EXTRA_ARGS="${@:2}"
  set --
fi

# default behaviour is to launch squid
if [[ -z ${1} ]]; then
  if [[ ! -d ${SQUID_CACHE_DIR}/00 ]]; then
    echo "Initializing cache..."
    ${SQUID} -N -f /etc/squid/squid.conf -z
  fi
  echo "Starting squid..."
  exec ${SQUID} -f /etc/squid/squid.conf -NYCd 1 ${EXTRA_ARGS}
else
  exec "$@"
fi
