#!/usr/bin/env bash

set -e

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P )"
THIS_DIR="${THIS_DIR:?}"

CERTS_DIR="${CERTS_DIR}"
CLIENT_DIR="${CERTS_DIR}/client"

PRIV="${CERTS_DIR}/cert.pem"
#CRT="${CERTS_DIR}/cert.crt"
CLIENT="${CLIENT_DIR}/cert.crt"
CONF="/certs.conf"
SIZE=2048

FILES=( "${PRIV}" "${CLIENT}" )

mkdir -p "${CLIENT_DIR}"

openssl req \
  -config "${CONF}" \
  -new \
  -newkey rsa:${SIZE} \
  -sha256 \
  -days 36500 \
  -nodes \
  -x509 \
  -extensions v3_ca \
  -keyout "${PRIV}" \
  -out "${PRIV}"

chmod 700 "${PRIV}"

openssl x509 -in "${PRIV}" -inform PEM -out "${CLIENT}"
#cp "${CRT}" "${CLIENT}"

for i in "${FILES[@]}"; do
  chown root "${i}"
done
